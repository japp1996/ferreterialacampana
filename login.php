<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Sistema | Ferretería la Campana</title>
    <meta name="description" content="Área interna para los asociados de Ferbric" />

    <link href="css/layout.css" rel="stylesheet" type="text/css" media="screen" />
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
    <link rel="apple-touch-icon" sizes="57x57" href="404.php">
    <link rel="apple-touch-icon" sizes="60x60" href="404.php">
    <link rel="apple-touch-icon" sizes="72x72" href="404.php">
    <link rel="apple-touch-icon" sizes="76x76" href="404.php">
    <link rel="apple-touch-icon" sizes="114x114" href="404.php">
    <link rel="apple-touch-icon" sizes="120x120" href="404.php">
    <link rel="apple-touch-icon" sizes="144x144" href="404.php">
    <link rel="apple-touch-icon" sizes="152x152" href="404.php">
    <link rel="apple-touch-icon" sizes="180x180" href="404.php">
    <link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="android-chrome-192x192.png" sizes="192x192">
    <link rel="icon" type="image/png" href="404.php" sizes="96x96">
    <link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="manifest.json">
    <link rel="mask-icon" href="safari-pinned-tab.svg">

    <script src="js/jquery-2.2.4.min.js"></script>
    <script src="js/axios.min.js"></script>
	<script src="js/sweetalert2.min.js"></script>

    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-TileImage" content="mstile-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <style>
        #wrapper{background-color:#FFF;height:100vh;}
        .loginpanel{position: absolute; top:50%; left: 50%; width: 300px; height: 400px; margin-top: -200px; margin-left: -150px; text-align: center;}
        .loginpanel img{margin:0 auto 2em auto;}
        .loginpanel h1{font-size:1.6em; margin: 0 0 1em;padding:0;}
    </style>
</head>
<body>
<div class="generalContent">
	<div id="wrapper" >
	    <div id="content">
			<div class="loginpanel">
				<img width="100%" src="images/logos/logo.png"/>
                <h2 class="" style="color:#000">Iniciar Sesión</h2>
                <hr style="margin: 1px 0 1px 0">
				<form action="php/loginController.php" role="form" id="login" method="POST" >
					<ul>
						<li class="full"><input type="email" name="email" data-name="Email" placeholder="Email:" size="10"></li>
						<li class="full"><input type="password" name="contrasena" data-name="Contraseña:" placeholder="Contraseña"></li>

                        <li class="full"><div class="alert alert-danger" id="errores" style="display:none;"></div></li>
						<li class="full"><input value="Entrar" type="button" onclick="login()" class="btn btn-default"></li>
                        <li class="full"><br><p><a href="forgot">¿Olvidaste tu Contraseña?</a></p></li>

                        <li class="full"><a href="register" class="btn btn-default">Registrarse</a></li>
                    </ul>
				</form>
			</div>
		</div>
	</div>
</div>

<script>
    function login() {
        let form = $('#login').serializeArray()
        let data = {}
        for (let i = 0; i < form.length; i++) {0
            data[form[i].name] = form[i].value
            console.log($('input[name='+form[i].name+']').data('name'))
            if (form[i].value == "") {
                Swal.fire("El campo " + $('input[name='+form[i].name+']').data('name') + " es requerido")
                return false
            }
        }

        axios.post('php/loginController.php', data)
        .then(res => {
            if (res.data.result) {
                window.location = res.data.msg+'/'
            } else {
                Swal.fire('Estimado usuario!', res.data.msg, 'info')
            }
        })
        .catch(err => {
            Swal.fire('Lo sentimos', 'En estos momentos no podemos procesar su solicitud', 'warning')
        })
    }
</script>
</body>

</html>