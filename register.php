<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8">
<meta content="width=device-width, initial-scale=1.0, minimum-scale=1" name="viewport">
<link rel="canonical" href="unirse-cadena-ferreterias.html" />
<link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="favicon-16x16.png">
<link rel="manifest" href="manifest.json">
<link rel="mask-icon" href="safari-pinned-tab.svg">
<meta name="theme-color" content="#ffffff">
<title>Registro | Ferreteria la Campana</title>
<meta name="keywords" content="" />
<meta name="description" content="Registrate para poder realizar tus pedidos de manera online"/>
<!-- Metas Open Graph -->
<link rel="image_src" href="share.jpg" />
<meta property="og:image" content="share.jpg"/>
<meta property="og:image:type" content="image/jpeg" />
<meta property="og:image:width" content="200" />
<meta property="og:image:height" content="200" />
<meta property="og:url" content="unirse-cadena-ferreterias.html" />
<meta property="og:title" content="Registro | Ferreteria la Campana" />
<meta property="og:description" content="Registrate para poder realizar tus pedidos de manera online" />
<!-- Metas Facebook -->
<!-- Twitter Card data -->
<meta name="twitter:title" content="Registro | Ferreteria la Campana">
<meta name="twitter:description" content="Registrate para poder realizar tus pedidos de manera online">
<meta name="twitter:image" content="share.jpg">
<link href="css/layout.css" rel="stylesheet" type="text/css" media="screen" />
<link rel="stylesheet" media="print" href="css/print.css">
<script src="js/jquery-3.1.1.min.js" type="text/javascript"></script>
<script src="js/functions.js" type="text/javascript"></script> <!-- Cargamos nuestros scripts generales -->
<script src="js/sweetalert2.min.js" type="text/javascript"></script> <!-- Cargamos nuestros scripts generales -->
<!--[if lt IE 9]>
    <script src="js/html5.js"></script>
<![endif]-->
</head>

<body class="es">
<?php include 'menu.php'?>
<div class="generalContent">
<!--section class="block1 background">
<div class="container">

	<h1 class="with-hr-right text-right" style="color:#fff">Únase a Ferbric</h1>
	<div class="text-right"><p class="subtitle" style="color:#fff">Solución de Competitividad para Ferreterías y establecimientos<br/> dedicados a la venta de artículos de Bricolaje, Jardín, Menaje<br/> y Hogar, Material de Construcción y Suministros Industriales</p></div>

</div>
</section>

<section class="block2 section-top-grey">
<div class="container">
	<div class="row">

		<div class="col-xs-12 col-sm-12 col-md-5 goup">
			<img src="images/branding/unase-01.jpg" alt="UNASE_ALT" class="img-responsive center-block">
		</div>
		<div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-1">
			<p><span>FERBRIC</span><span><span> </span>es una<span> </span></span><strong>cadena de ferreterías </strong>y establecimientos dedicados a la <strong>venta de productos de bricolaje</strong>, jardinería, menaje y hogar, material de construcción y suministros industriales<span>.<span> </span></span></p>
<p><span>Le ofrecemos<span> </span></span><span>soluciones para su negocio</span><span>. Soluciones que le permitirán hacer frente a la competencia, aunando esfuerzos con el objetivo común de ganar en<span> competitividad</span></span><span>.<span> </span></span></p>
<p><span>FERBRIC</span><span><span> </span>es la respuesta a un entorno caracterizado por la progresiva e imparable incursión de grandes superficies, cadenas u otras formas que permiten acceder a unas mejores condiciones de negociación con proveedores, así como conseguir mayores cuotas de mercado.<span> </span></span></p>
<h3><span>Únase a FERBRIC</span><span><span> </span></span></h3>
<p><span>Solicite más información sobre la rentabilidad que supone para su negocio, su incorporación a FERBRIC.<span> </span></span><a href="#formulario">Formulario de Asociación</a><span><span> </span></span></p>
<p><span>Con nosotros llegará<span> </span></span><span>MÁS LEJOS</span><span>. Solicite información sobre cómo podemos ayudarle a<span> </span></span><span>CRECER</span></p>		</div>

	</div>
</div>
</section-->
<section id="formulario">
<div class="container">
	<div class="row">
		<h2 class="with-hr-center" style="color:#000">Create una cuenta</h2>
		<div class="text-center"><p class="subtitle">Una vez completado el registro podrá ingresar al Sistema de pedidos de<br /> Ferretería la Campana, consulte nuestros productos y aparte los que necesite.</p></div>
		<div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2">
		<div class="row">
		<form action="" class="form01" id="registro" name="registro" method="post" >
			<input type="hidden" name="form" id="form" value="form" />
			<input type="hidden" name="url" id="url" value="contacto-ok.html" />
			<input type="hidden" name="subject" id="subject" value="Envío de formulario de contacto Ferretería la Campana"/>
			<ul>
				<li class="col-xs-12 col-sm-12 col-md-6">
					<label>Tipo de documento*</label>
					<select name="tipodedoc" data-name="Tipo de documento" type="text" id="tipodedoc">
						<option value="V">V - Venezolano</option>
						<option value="E">E - Extranjero</option>
						<option value="J">J - Jurídico</option>
						<option value="G">G - Gubernamental</option>
					</select>
				</li>

				<li class="col-xs-12 col-sm-12 col-md-6"><input data-name="Cedula" name="cedula" type="text" id="cedula" placeholder="Número de documento*" required/></li>

				<li class="col-xs-12 col-sm-12 col-md-6"><input data-name="Nombre" name="nombre" type="text" id="nombre" placeholder="Nombre o Razon Social*" required/></li>

				<li class="col-xs-12 col-sm-12 col-md-6"><input data-name="Contraseña" name="contrasena" type="password" id="contrasena" placeholder="Contraseña*" required/></li>

				<li class="col-xs-12 col-sm-12 col-md-6"><input data-name="Confirmacion de contraseña" name="repcontrasena" type="password" id="repcontrasena" placeholder="Repita la Contraseña*" required/></li>

				<li class="col-xs-12 col-sm-12 col-md-6"><input data-name="Email" name="email" type="email" id="email" placeholder="correo*" required/></li>

				<li class="col-xs-12 col-sm-12 col-md-6"><input data-name="Telefono" name="telefono" type="tel" id="telefono" placeholder="Teléfono*" required/></li>

				<li class="col-xs-12 col-sm-12 col-md-6"><input type="text" data-name="Pregunta de seguridad 1" name="question_one" placeholder="Pregunta de seguridad 1" class="input100"></li>
				
				<li class="col-xs-12 col-sm-12 col-md-6"><input type="text" data-name="Respuesta 1" name="answer_one" placeholder="Respuesta 1" class="input100"></li>

				<li class="col-xs-12 col-sm-12 col-md-6"><input type="text" data-name="Pregunta de seguridad 2" name="question_two" placeholder="Pregunta de seguridad 2" class="input100"></li>
				
				<li class="col-xs-12 col-sm-12 col-md-6"><input type="text" data-name="Respuesta 2" name="answer_two" placeholder="Respuesta 2" class="input100"></li>

				<li class="col-xs-12 col-sm-12"><textarea data-name="Direccion" name="direccion" type="tel" id="direccion" placeholder="Dirección*" required/></textarea></li>

				<li class="col-xs-12 col-sm-12 col-md-12"><input data-name="Fecha de nacimiento" name="fechanac" type="date" id="fechanac" placeholder="fecha*" required/></li>
				
				<li class="col-xs-12 col-sm-12">
					<input type="hidden" name="iso_idioma" id="iso_idioma" value="es" />
					<div class="text-center"><input value="Enviar" class="btn btn-submit btn-lg" onclick="register()" name="button" type="button" id="button"></div>
				</li>
			</ul>
		</form>
		</div>
		</div>
	</div>
</div>
</section>

<script>
	$("input:text").focus(function(){
		if($(this).val() == $(this).attr('name')){
			$(this).val('');
		}
	});
	$("input:text").blur(function(){
		if($(this).val() == ""){
			$(this).val($(this).attr('name'));
		}
	});
</script>
<?php include 'pie.php'?>
</div>
<!-- end generalContent -->
<script src="js/axios.min.js"></script>
<script src="js/sweetalert2.min.js"></script>
</body>
<script type="text/javascript">
	function register() {
		let form = $('#registro').serializeArray()
		let data = {}
		console.log(form)
		for (let i = 0; i < form.length; i++) {0
			data[form[i].name] = form[i].value
			console.log($('input[name='+form[i].name+']').data('name'))
			if (form[i].value == "") {
				Swal.fire("El campo " + $('input[name='+form[i].name+']').data('name') + " es requerido")
				return false
			}
		}

		axios.post('php/registerController.php', data)
		.then(res => {
			if (res.data.result) {
				Swal.fire('Excelente!', 'Usted ha sido registrado exitosamente!', 'success')
				setTimeout(() => {
					window.location = "login"
				}, 2000);
			} else {
				Swal.fire('Estimado usuario!', res.data.msg, 'info')
			}
		})
		.catch(err => {
			Swal.fire('Lo sentimos', 'En estos momentos no podemos procesar su solicitud', 'warning')
		})
	}
</script>
</html>

